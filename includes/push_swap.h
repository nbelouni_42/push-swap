/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/29 12:16:03 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/16 22:27:37 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <libft.h>

typedef struct		s_lst
{
	int				nbr;
	struct s_lst	*prev;
	struct s_lst	*next;
}					t_lst;

int					condition2(t_lst *lst, int a, int b);
int					condition3(t_lst *lst, int n, int zero);

int					rot
					(t_lst **lst, int (*condition)(t_lst *, int, int), int v);

int					call_rot(t_lst **lst, int n, int v);
int					call_rev_rot(t_lst **lst, int n, int v);
int					call_swap(t_lst **lst, int n, int v);
int					call_push(t_lst **lst1, t_lst**lst2, int n, int v);

int					insertion_sort(t_lst **lst, int v);
int					bubble_sort(t_lst **lst, int v);
int					merge_sort2(t_lst **lst, int v);

int					count_elem(t_lst *lst);
int					is_sorted(t_lst *lst);
int					is_max(t_lst *lst, int nbr);
int					is_min(t_lst *lst, int nbr);

t_lst				*swap(t_lst *list);
t_lst				*push(t_lst **lst1, t_lst *lst2);
t_lst				*rotate(t_lst *list);
t_lst				*rev_rotate(t_lst *list);

t_lst				*new_lst(char **argv);
t_lst				*free_list(t_lst *list);

int					aff_list(t_lst *a);
int					aff_piles(t_lst *l_a, t_lst *l_b);
int					aff_coups(int cp);
int					aff_arg_error(void);

int					push_swap(char **argv, int argc);
int					ft_check_arg(char **argv, int **tab);

#endif
