# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/30 11:19:25 by nbelouni          #+#    #+#              #
#    Updated: 2015/05/16 18:09:24 by nbelouni         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME= push_swap
CC= gcc
CFLAGS= -Wall -Werror -Wextra
HFLAGS= -I includes
LFLAGS= -I libft/includes/
SRC= src/lst.c \
	 src/move.c \
	 src/push_swap.c \
	 src/main.c \
	 src/aff_stuff.c \
	 src/checks.c \
	 src/insertionsort.c \
	 src/bubblesort.c \
	 src/call_move.c \
	 src/conditions.c \
	 src/ft_get_arg.c \
	 src/mergesort.c 
OBJ= lst.o move.o push_swap.o main.o checks.o insertionsort.o aff_stuff.o bubblesort.o call_move.o conditions.o mergesort.o ft_get_arg.o

all: $(NAME)

$(NAME): $(OBJ) 
	$(CC) -o $(NAME) $(OBJ) -L libft -lft

$(OBJ): $(SRC)
	make -C libft
	$(CC) $(CFLAGS) $(HFLAGS) $(LFLAGS) -c $(SRC)

clean:
	rm -f $(OBJ)
	make -C libft clean

fclean: clean
	rm -f $(NAME)
	make -C libft fclean

re: clean all
