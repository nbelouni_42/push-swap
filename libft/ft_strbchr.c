/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/13 21:42:40 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/14 03:47:03 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*strbchr(char *s, int c)
{
	int		i;
	char	*new;

	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			break ;
		i++;
	}
	new = (char *)malloc(sizeof(char) * i);
	ft_strncpy(new, s, i);
	new[i] = '\0';
	return (new);
}
