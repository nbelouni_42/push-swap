/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 01:40:01 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/24 17:46:21 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoin(char const *s1, char const *s2)
{
	int		j;
	int		k;
	char	*new;

	j = 0;
	k = 0;
	if (!s1 || !s2)
		return (NULL);
	new = (char *)malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1));
	if (!new)
		return (NULL);
	while (s1[j])
	{
		new[j] = s1[j];
		j++;
	}
	while (s2[k])
	{
		new[j] = s2[k];
		j++;
		k++;
	}
	new[j] = '\0';
	return (new);
}
