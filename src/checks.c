/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/08 20:12:43 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/15 20:17:51 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			count_elem(t_lst *lst)
{
	t_lst	*tmp;
	int		n_elem;

	if (!lst)
		return (0);
	tmp = lst->next;
	if (tmp->nbr == lst->nbr)
		return (1);
	n_elem = 1;
	while (tmp->nbr != lst->nbr)
	{
		tmp = tmp->next;
		n_elem++;
	}
	return (n_elem);
}

int			is_sorted(t_lst *lst)
{
	t_lst	*tmp;
	int		ref;

	ref = 0;
	tmp = lst;
	while (!ref)
	{
		if (tmp->next->nbr == lst->nbr)
			ref = 1;
		if (tmp->next->nbr != lst->nbr && tmp->nbr > tmp->next->nbr)
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

int			is_max(t_lst *lst, int nbr)
{
	t_lst	*tmp;

	tmp = lst;
	if (nbr < tmp->prev->nbr)
		return (0);
	while (tmp->next->nbr != lst->nbr)
	{
		if (nbr < tmp->nbr)
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

int			is_min(t_lst *lst, int nbr)
{
	t_lst	*tmp;

	tmp = lst;
	if (nbr > tmp->prev->nbr)
		return (0);
	while (tmp->next->nbr != lst->nbr)
	{
		if (nbr > tmp->nbr)
			return (0);
		tmp = tmp->next;
	}
	return (1);
}
