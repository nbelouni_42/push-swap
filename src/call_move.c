/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   call_move.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 13:43:03 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/16 20:30:37 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		call_rot(t_lst **lst, int n, int v)
{
	*lst = rotate(*lst);
	if (v)
		ft_putstr((!n) ? "ra " : "rb ");
	return (1);
}

int		call_rev_rot(t_lst **lst, int n, int v)
{
	*lst = rev_rotate(*lst);
	if (v)
		ft_putstr((!n) ? "rra " : "rrb ");
	return (1);
}

int		call_swap(t_lst **lst, int n, int v)
{
	*lst = swap(*lst);
	if (v)
		ft_putstr((!n) ? "sa " : "sb ");
	return (1);
}

int		call_push(t_lst **lst1, t_lst **lst2, int n, int v)
{
	*lst1 = push(lst2, *lst1);
	if (v)
		ft_putstr((!n) ? "pa " : "pb ");
	return (1);
}
