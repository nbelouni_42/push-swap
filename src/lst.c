/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/29 12:22:46 by nbelouni          #+#    #+#             */
/*   Updated: 2015/06/03 15:12:22 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				check_numbers(char **argv)
{
	long int	test;
	int			i;
	int			j;

	i = 0;
	while (argv[i])
	{
		j = 1;
		if (ft_strlen(argv[i]) > 11 ||
		(!ft_isdigit(argv[i][0]) && argv[i][0] != '-' && argv[i][0] != '+'))
			return (0);
		test = ft_atol(argv[i]);
		if (test > 2147483647 || test < -2147483648)
			return (0);
		while (argv[i][j])
		{
			if (!ft_isdigit(argv[i][j]))
				return (0);
			j++;
		}
		if (j > 11)
			return (0);
		i++;
	}
	return (1);
}

int				check_doubles(char **argv)
{
	int			i;
	int			j;

	i = 0;
	while (argv[i])
	{
		j = i + 1;
		while (argv[j])
		{
			if (!strcmp(argv[i], argv[j]))
			{
				ft_putendl(argv[i]);
				return (0);
			}
			j++;
		}
		i++;
	}
	return (1);
}

t_lst			*free_list(t_lst *list)
{
	t_lst		*tmp;

	if (list && list->nbr)
	{
		tmp = list->prev;
		tmp->next = NULL;
		list->prev = NULL;
		while (list)
		{
			tmp = list;
			free(tmp);
			list = list->next;
			tmp = NULL;
		}
	}
	return (NULL);
}

t_lst			*new_elem(char *s)
{
	t_lst		*new;

	if (!(new = (t_lst *)malloc(sizeof(t_lst))))
		return (NULL);
	new->nbr = ft_atoi(s);
	new->prev = NULL;
	new->next = NULL;
	return (new);
}

t_lst			*new_lst(char **argv)
{
	t_lst		*top;
	t_lst		*list;
	t_lst		*front;
	int			i;

	if (!check_numbers(argv) || !check_doubles(argv))
		return (NULL);
	top = new_elem(argv[0]);
	list = top;
	if (!argv[1])
		return (list);
	i = 1;
	while (argv[i])
	{
		front = new_elem(argv[i]);
		front->prev = list;
		list->next = front;
		list = front;
		i++;
	}
	top->prev = front;
	front->next = top;
	return (top);
}
