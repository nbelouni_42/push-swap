/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conditions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 14:13:53 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/16 20:33:46 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			condition2(t_lst *lst, int a, int b)
{
	(void)lst;
	return (a < b);
}

int			condition3(t_lst *lst, int n, int zero)
{
	(void)zero;
	return (is_max(lst, n));
}
