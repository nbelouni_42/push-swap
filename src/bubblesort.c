/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bubblesort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/08 23:50:15 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/16 22:19:26 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

int				find_rot(t_lst **a, int len, int v)
{
	t_lst	*tmp;
	int		i;
	int		cp;

	tmp = *a;
	i = 0;
	cp = 0;
	while (++i < len && !is_sorted(*a) && tmp->nbr <= tmp->next->nbr)
		tmp = tmp->next;
	if (i < (len / 2))
		while (i > 0 && !is_sorted(*a))
		{
			cp += call_rot(a, 0, v);
			i--;
		}
	else if (i >= (len / 2))
		while (i < len && !is_sorted(*a))
		{
			cp += call_rev_rot(a, 0, v);
			i++;
		}
	return (cp);
}

int				check_rot_bubble(t_lst *a, int len)
{
	t_lst	*tmp;
	int		i;

	i = 0;
	tmp = a;
	while (i < len)
	{
		if (is_max(a, tmp->nbr) && is_min(a, tmp->next->nbr))
		{
			tmp = tmp->next;
			i++;
		}
		else if (tmp->nbr > tmp->next->nbr)
			break ;
		i++;
		tmp = tmp->next;
	}
	return (i);
}

static int		rot_bubble(t_lst **a, int len, int v)
{
	int		i;
	int		cp;

	cp = 0;
	i = check_rot_bubble(*a, len);
	if (i && i < (len / 2))
		while (i > 0 && !is_sorted(*a))
		{
			cp += call_rot(a, 0, v);
			i--;
		}
	else if (i == len)
		cp += find_rot(a, len, v);
	else if (i >= (len / 2))
		while (i < len && !is_sorted(*a))
		{
			cp += call_rev_rot(a, 0, v);
			i++;
		}
	return (cp);
}

int				bubble_sort(t_lst **lst, int v)
{
	t_lst	*a;
	int		cp;
	int		len;

	a = *lst;
	cp = 0;
	len = count_elem(*lst);
	while (!is_sorted(a))
	{
		if ((!is_min(a, a->next->nbr) || !is_max(a, a->nbr))
				&& a->nbr > a->next->nbr)
		{
			cp += call_swap(&a, 0, v);
		}
		if (is_sorted(a))
			break ;
		else
		{
			cp += rot_bubble(&a, len, v);
		}
	}
	*lst = a;
	return (cp);
}
