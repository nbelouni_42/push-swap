/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mergesort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 19:36:59 by nbelouni          #+#    #+#             */
/*   Updated: 2015/06/03 14:22:37 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	rot1_merge(t_lst **a, int v)
{
	t_lst	*tmp;
	int		k;
	int		cp;

	cp = 0;
	k = -1;
	tmp = *a;
	while (k++, !is_min(*a, tmp->nbr))
		tmp = tmp->next;
	if (k > (count_elem(*a) / 2))
		while (k < count_elem(*a))
		{
			cp += call_rev_rot(a, 0, v);
			k++;
		}
	else
	{
		while (k > 0)
		{
			cp += call_rot(a, 0, v);
			k--;
		}
	}
	return (cp);
}

static int	rot2_merge(t_lst **a, t_lst *b, int v)
{
	t_lst	*tmp;
	int		k;
	int		cp;

	cp = 0;
	k = -1;
	tmp = *a;
	while (k++, tmp->prev->nbr > b->nbr)
		tmp = tmp->prev;
	if (k > (count_elem(*a) / 2))
		while (k < count_elem(*a))
		{
			cp += call_rot(a, 0, v);
			k++;
		}
	else
	{
		while (k > 0)
		{
			cp += call_rev_rot(a, 0, v);
			k--;
		}
	}
	return (cp);
}

static int	rot3_merge(t_lst **a, t_lst *b, int v)
{
	t_lst	*tmp;
	int		k;
	int		cp;

	cp = 0;
	k = -1;
	tmp = *a;
	while (++k < count_elem(*a))
	{
		if (b->nbr < tmp->nbr && b->nbr > tmp->prev->nbr)
			break ;
		tmp = tmp->next;
	}
	if (k < (count_elem(*a) / 2))
		while (k-- > 0)
			cp += call_rot(a, 0, v);
	else
	{
		while (k < count_elem(*a))
		{
			cp += call_rev_rot(a, 0, v);
			k++;
		}
	}
	return (cp);
}

static int	merge_b(t_lst **a, t_lst **b, int v)
{
	int		cp;

	cp = 0;
	while (*b)
	{
		if (is_max(*a, (*b)->nbr))
			cp += call_push(a, b, 0, v);
		else if (is_min(*a, (*b)->nbr))
		{
			cp += rot1_merge(a, v);
			cp += call_push(a, b, 0, v);
		}
		else if ((*b)->nbr < (*a)->nbr)
		{
			cp += rot2_merge(a, *b, v);
			cp += call_push(a, b, 0, v);
		}
		else
			cp += rot3_merge(a, *b, v);
	}
	return (cp);
}

int			merge_sort2(t_lst **lst, int v)
{
	t_lst	*a;
	t_lst	*b;
	int		len;
	int		cp;

	a = *lst;
	b = NULL;
	cp = 0;
	len = count_elem(*lst) / 2;
	while (len > 0)
	{
		if (a->nbr < a->next->nbr)
		{
			cp += call_push(&b, &a, 1, v);
			len--;
		}
		else
			cp += call_swap(&a, 0, v);
	}
	cp += insertion_sort(&a, v);
	cp += merge_b(&a, &b, v);
	cp += rot1_merge(&a, v);
	*lst = a;
	return (cp);
}
