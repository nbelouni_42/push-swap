/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/04 21:45:42 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/09 05:37:49 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_lst		*swap(t_lst *lst)
{
	t_lst	*tmp;
	t_lst	*tmp2;
	t_lst	*tmp3;

	if (lst->next->nbr == lst->nbr)
		return (lst);
	tmp = lst->next;
	if (tmp->next != lst)
	{
		tmp2 = lst->prev;
		tmp3 = tmp->next;
		tmp->prev = tmp2;
		lst->prev = tmp;
		tmp3->prev = lst;
		tmp2->next = tmp;
		lst->next = tmp3;
		tmp->next = lst;
	}
	else
		tmp = rotate(lst);
	return (tmp);
}

t_lst		*push_need(t_lst **lst2, t_lst *tmp)
{
	t_lst	*tmp3;

	tmp3 = (*lst2)->prev;
	(*lst2)->prev = tmp;
	tmp->next = *lst2;
	tmp3->next = tmp;
	tmp->prev = tmp3;
	return (tmp);
}

t_lst		*push(t_lst **lst1, t_lst *lst2)
{
	t_lst	*tmp;
	t_lst	*tmp2;

	if (!*lst1)
		return (lst2);
	tmp = *lst1;
	tmp2 = (*lst1)->prev;
	*lst1 = (*lst1)->next;
	if (!lst2)
	{
		tmp->next = tmp;
		tmp->prev = tmp;
	}
	else if (tmp->nbr == tmp->next->nbr)
	{
		tmp = push_need(&lst2, tmp);
		lst2->prev = tmp;
		(*lst1) = NULL;
		return (tmp);
	}
	else
		tmp = push_need(&lst2, tmp);
	tmp2->next = *lst1;
	(*lst1)->prev = tmp2;
	return (tmp);
}

t_lst		*rotate(t_lst *lst)
{
	t_lst	*tmp;

	tmp = lst->next;
	return (tmp);
}

t_lst		*rev_rotate(t_lst *lst)
{
	t_lst	*tmp;

	tmp = lst->prev;
	return (tmp);
}
