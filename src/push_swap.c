/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/29 12:32:08 by nbelouni          #+#    #+#             */
/*   Updated: 2015/06/03 15:20:03 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			all_sorts(t_lst **a, char **argv, int *tab, int **cps)
{
	if (((!tab[2] && !tab[3]) && count_elem(*a) < 200) || tab[1] || tab[4])
	{
		ft_putstr((tab[0]) ? "\033[35mbubble_sort :\n\033[0m" : "");
		if ((cps[0][0] = bubble_sort(a, tab[0])) == 0)
			return (0);
		*a = free_list(*a);
		*a = new_lst(argv);
	}
	else
		cps[0][0] = -1;
	if ((!tab[1] && !tab[3]) || tab[2] || tab[4])
	{
		ft_putstr((tab[0]) ? "\033[36m\ninsertion_sort :\n\033[0m" : "");
		cps[0][1] = insertion_sort(a, tab[0]);
		*a = free_list(*a);
	}
	if ((!tab[1] && !tab[2]) || tab[3] || tab[4])
	{
		*a = new_lst(argv);
		ft_putstr((tab[0]) ? "\033[33m\nmerge_sort_sort :\n\033[0m" : "");
		cps[0][2] = merge_sort2(a, tab[0]);
		*a = free_list(*a);
	}
	return (1);
}

int			coups_min(int *cps)
{
	if (cps[0] > -1 && cps[0] <= cps[1] && cps[0] <= cps[2])
		return (cps[0]);
	if (cps[1] <= cps[2])
		return (cps[1]);
	return (cps[2]);
}

int			choose_sort(int *tab, int cps[3])
{
	if (tab[1] || tab[4])
	{
		ft_putstr("\033[35m\nbubble_sort : \033[0m");
		aff_coups(cps[0]);
	}
	if (tab[2] || tab[4])
	{
		ft_putstr("\033[36m\ninsertion_sort : \033[0m");
		aff_coups(cps[1]);
	}
	if (tab[3] || tab[4])
	{
		ft_putstr("\033[33m\nmergesort : \033[0m");
		aff_coups(cps[2]);
	}
	return (0);
}

int			push_swap(char **argv, int argc)
{
	t_lst	*a;
	int		*cps;
	int		*tab;
	int		i;

	if (!(tab = (int *)malloc(sizeof(int) * 5)))
		return (0);
	if (!(cps = (int *)malloc(sizeof(int) * 3)))
		return (0);
	if ((i = ft_check_arg(argv, &tab)) < 0)
		return (aff_arg_error());
	if (i >= argc || !(a = new_lst(argv + (i))))
		return (-1);
	if (!a->next)
		return (aff_coups(0));
	aff_list(a);
	if (!(all_sorts(&a, argv + i, tab, &cps)))
		return (aff_coups(0));
	if (!tab[1] && !tab[2] && !tab[3] && !tab[4])
	{
		ft_putstr("\n\n");
		return (aff_coups(coups_min(cps)));
	}
	choose_sort(tab, cps);
	return (0);
}
