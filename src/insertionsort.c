/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insertionsort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/08 19:26:51 by nbelouni          #+#    #+#             */
/*   Updated: 2015/05/16 18:42:19 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			rot(t_lst **lst, int (*condition)(t_lst *, int, int), int v)
{
	t_lst	*tmp;
	int		len;
	int		i;
	int		cp;

	len = count_elem(*lst);
	tmp = *lst;
	i = -1;
	cp = 0;
	while (tmp && ++i < len)
	{
		if (condition(*lst, tmp->nbr, (*lst)->nbr))
			break ;
		tmp = tmp->next;
	}
	if (i && i < (len / 2))
		while (--i > -1)
			cp += call_rot(lst, 1, v);
	else if (i >= (len / 2))
		while (++i < (len + 1))
			cp += call_rev_rot(lst, 1, v);
	return (cp);
}

int			push_b(t_lst **a, t_lst **b, int v)
{
	int		cp;

	cp = 0;
	while (*b)
	{
		cp += rot(b, condition3, v);
		if (is_min(*a, (*b)->nbr))
			cp += call_push(a, b, 0, v);
	}
	return (cp);
}

int			insertion_sort(t_lst **lst, int v)
{
	t_lst	*a;
	t_lst	*b;
	int		i;
	int		cp;
	int		len;

	a = *lst;
	b = NULL;
	cp = 0;
	len = count_elem(*lst);
	i = 0;
	while (i < (len - 1))
	{
		if (a->nbr < a->next->nbr)
		{
			cp += rot(&b, &condition2, v);
			cp += call_push(&b, &a, 1, v);
			i++;
		}
		else
			cp += call_swap(&a, 0, v);
	}
	cp += push_b(&a, &b, v);
	*lst = a;
	return (cp);
}
