/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_arg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/22 23:16:17 by nbelouni          #+#    #+#             */
/*   Updated: 2015/06/03 14:15:28 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int			*ft_init_tab(int *tab)
{
	int				i;

	i = 0;
	while (i < 5)
	{
		tab[i] = 0;
		i++;
	}
	return (tab);
}

static int			count_arg(int **tab, int c)
{
	if (c == 'v')
		tab[0][0] += 1;
	else if (c == 'b')
		tab[0][1] += 1;
	else if (c == 'i')
		tab[0][2] += 1;
	else if (c == 'm')
		tab[0][3] += 1;
	else if (c == 'a')
		tab[0][4] += 1;
	else if (ft_isdigit(c))
		return (1);
	else
		return (-1);
	return (0);
}

static int			stock_arg(char *argv, int **tab)
{
	int				i;
	int				b;

	i = 1;
	tab[0][5] = 0;
	while (argv[i])
	{
		if (argv[1] == '-')
			return (argv[2] != '\0' ? -1 : 1);
		if ((b = count_arg(tab, argv[i])) == -1)
			return (-1);
		if (!b)
			return (0);
		else if (b == 1)
			return (1);
		i++;
	}
	return (0);
}

int					ft_check_arg(char **argv, int **tab)
{
	int				i;
	int				check;

	i = 1;
	*tab = ft_init_tab(*tab);
	while (argv[i] && argv[i][0] == '-')
	{
		if (!argv[i][1])
			return (i);
		if (argv[i][0] == '-')
			check = stock_arg(argv[i], tab);
		if (check == -1)
			return (-1);
		else if (check > 0)
			return (i);
		else if (check == 0)
			i++;
	}
	return (i);
}
