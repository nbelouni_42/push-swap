/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/29 12:20:32 by nbelouni          #+#    #+#             */
/*   Updated: 2015/06/03 14:10:55 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			main(int argc, char **argv)
{
	if (argc == 1)
		ft_putendl_fd("Error", 2);
	else
	{
		if (push_swap(argv, argc) == -1)
			ft_putendl_fd("Error", 2);
		else
			write(1, "\n", 1);
	}
	return (0);
}
