/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_stuff.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/14 15:26:46 by nbelouni          #+#    #+#             */
/*   Updated: 2015/06/03 15:20:05 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		aff_list(t_lst *a)
{
	t_lst	*tmp1;
	int		ref;
	int		lst_end;

	lst_end = 0;
	ref = a->nbr;
	tmp1 = a;
	while (lst_end == 0)
	{
		ft_putnbr(tmp1->nbr);
		ft_putchar(' ');
		tmp1 = tmp1->next;
		if (tmp1->nbr == ref)
			lst_end = 1;
	}
	write(1, "\n", 1);
	return (1);
}

int		aff_piles(t_lst *l_a, t_lst *l_b)
{
	ft_putstr("\nl_a : ");
	if (l_a)
		aff_list(l_a);
	ft_putstr("\nl_b : ");
	if (l_b)
		aff_list(l_b);
	return (1);
}

int		aff_coups(int cp)
{
	ft_putstr("nombre de cp : ");
	ft_putnbr(cp);
	write(1, "\n", 1);
	return (1);
}

int		aff_arg_error(void)
{
	ft_putendl("OPTIONS :");
	ft_putendl("	-v : mode verbose");
	ft_putendl("	-i : insertion_sort");
	ft_putendl("	-b : bubblesort");
	ft_putendl("	-m : mergesort");
	ft_putendl("	-a : tous les tris\n");
	ft_putendl("defaut : tri le plus optimise, verbose desactivee");
	return (0);
}
